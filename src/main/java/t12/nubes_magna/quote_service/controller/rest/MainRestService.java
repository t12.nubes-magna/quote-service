package t12.nubes_magna.quote_service.controller.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import t12.nubes_magna.quote_service.model.rest.QuoteDTO;

import java.util.List;

@RequestMapping("/quotes")
public interface MainRestService {

    @GetMapping(value = "/toApprove")
    List<QuoteDTO> getQuotesToApprove();

    @GetMapping
    List<QuoteDTO> geApprovedQuotes();

    @PutMapping
    QuoteDTO addNewQuote(@RequestBody final QuoteDTO newQuote);

    @PostMapping
    QuoteDTO approveQuote(@RequestParam(name = "quoteId") final int quoteId);


    @GetMapping(value = "/quoteOfDay")
    QuoteDTO getQuoteOfTheDay();

}
