package t12.nubes_magna.quote_service.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import t12.nubes_magna.quote_service.dao.QuoteDAO;
import t12.nubes_magna.quote_service.model.ApprovedQuote;
import t12.nubes_magna.quote_service.model.QuoteToApprove;
import t12.nubes_magna.quote_service.model.converter.ApprovedQuote2QuoteDTO;
import t12.nubes_magna.quote_service.model.converter.QuoteDTO2QuoteToApprove;
import t12.nubes_magna.quote_service.model.converter.QuoteToApprove2QuoteDTO;
import t12.nubes_magna.quote_service.model.rest.QuoteDTO;
import t12.nubes_magna.quote_service.service.QuoteOfTheDayService;

import java.util.ArrayList;
import java.util.List;

@RestController
public class MainRestServiceImpl implements MainRestService {

    @Autowired
    private QuoteDAO quoteDAOImpl;

    @Autowired
    private QuoteOfTheDayService quoteOfTheDayService;

    private QuoteToApprove2QuoteDTO quoteToApprove2QuoteDTO = new QuoteToApprove2QuoteDTO();
    private ApprovedQuote2QuoteDTO approvedQuote2QuoteDTO = new ApprovedQuote2QuoteDTO();
    private QuoteDTO2QuoteToApprove quoteDTO2QuoteToApprove = new QuoteDTO2QuoteToApprove();

    @Override
    public List<QuoteDTO> getQuotesToApprove() {
        final List<QuoteToApprove> quotesToApprove = quoteDAOImpl.getQuotesToApprove();
        final List<QuoteDTO> quoteDTOList = new ArrayList<>();
        quotesToApprove.forEach(quoteToApprove -> quoteDTOList.add(quoteToApprove2QuoteDTO.convert(quoteToApprove)));
        return quoteDTOList;
    }

    @Override
    public List<QuoteDTO> geApprovedQuotes() {
        final List<ApprovedQuote> approvedQuotes = quoteDAOImpl.getAllApprovedQuotes();
        final List<QuoteDTO> quoteDTOList = new ArrayList<>();
        approvedQuotes.forEach(approvedQuote -> quoteDTOList.add(approvedQuote2QuoteDTO.convert(approvedQuote)));
        return quoteDTOList;
    }

    @Override
    public QuoteDTO addNewQuote(@RequestBody final QuoteDTO newQuote) {
        final QuoteToApprove quoteToApprove = quoteDAOImpl.addNewQuote(quoteDTO2QuoteToApprove.convert(newQuote));

        return quoteToApprove2QuoteDTO.convert(quoteToApprove);
    }

    @Override
    public QuoteDTO approveQuote(@RequestParam(name = "quoteId") final int quoteId) {
        final ApprovedQuote approvedQuote = quoteDAOImpl.approveQuote(quoteId);

        return approvedQuote2QuoteDTO.convert(approvedQuote);
    }

    @Override
    public QuoteDTO getQuoteOfTheDay() {
        return approvedQuote2QuoteDTO.convert(quoteOfTheDayService.getQuoteOfTheDay());
    }
}
