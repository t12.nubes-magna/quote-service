package t12.nubes_magna.quote_service.dao;

import t12.nubes_magna.quote_service.model.ApprovedQuote;
import t12.nubes_magna.quote_service.model.QuoteToApprove;

import java.util.List;

public interface QuoteDAO {

    List<ApprovedQuote> getAllApprovedQuotes();

    QuoteToApprove addNewQuote(final QuoteToApprove quoteToApprove);

    List<QuoteToApprove> getQuotesToApprove();

    ApprovedQuote approveQuote(final int quoteId);

    boolean disapproveQuote(final int quoteId);
}
