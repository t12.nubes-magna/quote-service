package t12.nubes_magna.quote_service.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import t12.nubes_magna.quote_service.model.ApprovedQuote;
import t12.nubes_magna.quote_service.model.QuoteToApprove;

import java.util.List;

@Repository
public class QuoteDAOImpl implements QuoteDAO {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public List<ApprovedQuote> getAllApprovedQuotes() {

        final StringBuilder sql = new StringBuilder();
        sql.append(" SELECT \n")
                .append(" id, \n")
                .append(" quote, \n")
                .append(" author, \n")
                .append(" submitter, \n")
                .append(" lastUsed \n")
                .append(" FROM ApprovedQuotes");

        final List<ApprovedQuote> quoteList = jdbcTemplate
                .query(sql.toString(), (rs, rowNum) -> new ApprovedQuote.ApprovedQuoteBuilder()
                        .setId(rs.getObject("id", Integer.class))
                        .setQuote(rs.getString("quote"))
                        .setAuthor(rs.getString("author"))
                        .setSubmitter(rs.getString("submitter"))
                        .setLastUsed(rs.getDate("lastUsed"))
                        .createQuote());

        return quoteList;
    }

    @Override
    public QuoteToApprove addNewQuote(final QuoteToApprove quoteToApprove) {

        final StringBuilder sql = new StringBuilder();
        sql.append(" INSERT INTO QuotesToApprove (quote, author, submitter) VALUES ( \n")
                .append(" :quote, \n")
                .append(" :author, \n")
                .append(" :submitter, \n")
                .append(" ) ");

        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(quoteToApprove);

        KeyHolder keyHolder = new GeneratedKeyHolder();

        final int insertedRows = jdbcTemplate.update(sql.toString(), parameterSource, keyHolder);

        if (insertedRows != 0) {
            return getQuoteToApprove(keyHolder.getKey().intValue());
        }

        return null;
    }


    @Override
    public List<QuoteToApprove> getQuotesToApprove() {

        final StringBuilder sql = new StringBuilder();
        sql.append(" SELECT \n")
                .append(" id, \n")
                .append(" quote, \n")
                .append(" author, \n")
                .append(" submitter \n")
                .append(" FROM QuotesToApprove");

        final List<QuoteToApprove> quoteList = jdbcTemplate
                .query(sql.toString(), (rs, rowNum) -> new QuoteToApprove.QuoteToApproveBuilder()
                        .setId(rs.getObject("id", Integer.class))
                        .setQuote(rs.getString("quote"))
                        .setAuthor(rs.getString("author"))
                        .setSubmitter(rs.getString("submitter"))
                        .createQuote());

        return quoteList;
    }

    @Override
    public ApprovedQuote approveQuote(final int quoteId) {
        final QuoteToApprove quoteToApprove = getQuoteToApprove(quoteId);
        if (quoteToApprove != null) {

            final StringBuilder sql = new StringBuilder();
            sql.append(" INSERT INTO ApprovedQuotes (quote, author, submitter) VALUES ( \n")
                    .append(" :quote, \n")
                    .append(" :author, \n")
                    .append(" :submitter, \n")
                    .append(" ) ");

            SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(quoteToApprove);

            KeyHolder keyHolder = new GeneratedKeyHolder();

            final int insertedRows = jdbcTemplate.update(sql.toString(), parameterSource, keyHolder);

            if (insertedRows != 0) {
                deleteQuoteToApprove(quoteId);
                return getApprovedQuote(keyHolder.getKey().intValue());
            }
        }
        return null;
    }

    @Override
    public boolean disapproveQuote(final int quoteId) {
        return deleteQuoteToApprove(quoteId);
    }

    private boolean deleteQuoteToApprove(final int quoteId) {

        final StringBuilder sql = new StringBuilder();
        sql.append(" DELETE \n")
                .append(" FROM QuotesToApprove \n")
                .append(" WHERE id = :id");

        SqlParameterSource parameterSource = new MapSqlParameterSource("id", quoteId);

        final int rowsAffected = jdbcTemplate.update(sql.toString(), parameterSource);

        return rowsAffected != 0;
    }


    private QuoteToApprove getQuoteToApprove(final int id) {
        final StringBuilder sql = new StringBuilder();
        sql.append(" SELECT \n")
                .append(" id, \n")
                .append(" quote, \n")
                .append(" author, \n")
                .append(" submitter \n")
                .append(" FROM QuotesToApprove")
                .append(" WHERE id = :id");

        SqlParameterSource parameterSource = new MapSqlParameterSource("id", id);

        return jdbcTemplate.queryForObject(sql
                .toString(), parameterSource, (rs, rowNum) -> new QuoteToApprove.QuoteToApproveBuilder()
                .setId(rs.getObject("id", Integer.class))
                .setQuote(rs.getString("quote"))
                .setAuthor(rs.getString("author"))
                .setSubmitter(rs.getString("submitter"))
                .createQuote());
    }

    private ApprovedQuote getApprovedQuote(final int id) {
        final StringBuilder sql = new StringBuilder();
        sql.append(" SELECT \n")
                .append(" id, \n")
                .append(" quote, \n")
                .append(" author, \n")
                .append(" submitter \n")
                .append(" FROM ApprovedQuotes")
                .append(" WHERE id = :id");

        SqlParameterSource parameterSource = new MapSqlParameterSource("id", id);

        return jdbcTemplate.queryForObject(sql
                .toString(), parameterSource, (rs, rowNum) -> new ApprovedQuote.ApprovedQuoteBuilder()
                .setId(rs.getObject("id", Integer.class))
                .setQuote(rs.getString("quote"))
                .setAuthor(rs.getString("author"))
                .setSubmitter(rs.getString("submitter"))
                .createQuote());
    }
}
