package t12.nubes_magna.quote_service.model;

import java.util.Date;

public class ApprovedQuote extends Quote {

    private static final long serialVersionUID = 7164528804204381188L;

    private Date lastUsed;

    protected ApprovedQuote(final Integer id,
                            final String quote,
                            final String author,
                            final String submitter,
                            final Date lastUsed) {
        super(id, quote, author, submitter);
        this.lastUsed = lastUsed;
    }

    public Date getLastUsed() {
        return lastUsed;
    }

    public void setLastUsed(final Date lastUsed) {
        this.lastUsed = lastUsed;
    }

    public static class ApprovedQuoteBuilder extends ApprovedQuote.QuoteBuilder<ApprovedQuoteBuilder> {

        private Date lastUsed;

        public ApprovedQuoteBuilder setLastUsed(final Date lastUsed) {
            this.lastUsed = lastUsed;
            return this;
        }

        public ApprovedQuote createQuote() {
            return new ApprovedQuote(super.id, super.quote, super.author, super.submitter, lastUsed);
        }
    }
}
