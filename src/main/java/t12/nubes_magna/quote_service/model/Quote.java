package t12.nubes_magna.quote_service.model;

import java.io.Serializable;

public abstract class Quote implements Serializable {

    private static final long serialVersionUID = 7509984443919463035L;

    private Integer id;
    private String quote;
    private String author;
    private String submitter;

    protected Quote(final Integer id, final String quote, final String author, final String submitter) {
        this.id = id;
        this.quote = quote;
        this.author = author;
        this.submitter = submitter;
    }

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(final String quote) {
        this.quote = quote;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(final String author) {
        this.author = author;
    }

    public String getSubmitter() {
        return submitter;
    }

    public void setSubmitter(final String submitter) {
        this.submitter = submitter;
    }

    protected abstract static class QuoteBuilder<T> {
        protected Integer id;
        protected String quote;
        protected String author;
        protected String submitter;

        public T setId(final Integer id) {
            this.id = id;
            return (T) this;
        }

        public T setQuote(final String quote) {
            this.quote = quote;
            return (T) this;
        }

        public T setAuthor(final String author) {
            this.author = author;
            return (T) this;
        }

        public T setSubmitter(final String submitter) {
            this.submitter = submitter;
            return (T) this;
        }
    }
}
