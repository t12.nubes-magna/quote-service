package t12.nubes_magna.quote_service.model;

public class QuoteToApprove extends Quote {

    private static final long serialVersionUID = -3046516780377134887L;

    protected QuoteToApprove(final Integer id,
                             final String quote,
                             final String author,
                             final String submitter) {
        super(id, quote, author, submitter);
    }

    public static class QuoteToApproveBuilder extends ApprovedQuote.QuoteBuilder<QuoteToApproveBuilder> {


        public QuoteToApprove createQuote() {
            return new QuoteToApprove(super.id, super.quote, super.author, super.submitter);
        }
    }
}
