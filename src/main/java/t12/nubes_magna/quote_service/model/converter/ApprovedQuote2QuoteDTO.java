package t12.nubes_magna.quote_service.model.converter;

import org.springframework.core.convert.converter.Converter;
import t12.nubes_magna.quote_service.model.ApprovedQuote;
import t12.nubes_magna.quote_service.model.rest.QuoteDTO;

public class ApprovedQuote2QuoteDTO implements Converter<ApprovedQuote, QuoteDTO> {

    @Override
    public QuoteDTO convert(final ApprovedQuote source) {
        return new QuoteDTO.QuoteDTOBuilder()
                .setId(source.getId())
                .setAuthor(source.getAuthor())
                .setQuote(source.getQuote())
                .setSubmitter(source.getSubmitter())
                .createQuoteDTO();
    }
}
