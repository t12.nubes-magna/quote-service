package t12.nubes_magna.quote_service.model.converter;

import org.springframework.core.convert.converter.Converter;
import t12.nubes_magna.quote_service.model.ApprovedQuote;
import t12.nubes_magna.quote_service.model.rest.QuoteDTO;

public class QuoteDTO2ApprovedQuote implements Converter<QuoteDTO, ApprovedQuote> {

    @Override
    public ApprovedQuote convert(final QuoteDTO source) {
        return new ApprovedQuote.ApprovedQuoteBuilder()
                .setAuthor(source.getAuthor())
                .setQuote(source.getQuote())
                .setSubmitter(source.getSubmitter())
                .createQuote();
    }
}
