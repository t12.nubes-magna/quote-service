package t12.nubes_magna.quote_service.model.converter;

import org.springframework.core.convert.converter.Converter;
import t12.nubes_magna.quote_service.model.QuoteToApprove;
import t12.nubes_magna.quote_service.model.rest.QuoteDTO;

public class QuoteDTO2QuoteToApprove implements Converter<QuoteDTO, QuoteToApprove> {

    @Override
    public QuoteToApprove convert(final QuoteDTO source) {
        return new QuoteToApprove.QuoteToApproveBuilder()
                .setAuthor(source.getAuthor())
                .setQuote(source.getQuote())
                .setSubmitter(source.getSubmitter())
                .createQuote();
    }
}
