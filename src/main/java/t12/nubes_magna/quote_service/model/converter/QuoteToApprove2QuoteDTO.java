package t12.nubes_magna.quote_service.model.converter;

import org.springframework.core.convert.converter.Converter;
import t12.nubes_magna.quote_service.model.QuoteToApprove;
import t12.nubes_magna.quote_service.model.rest.QuoteDTO;

public class QuoteToApprove2QuoteDTO implements Converter<QuoteToApprove, QuoteDTO> {

    @Override
    public QuoteDTO convert(final QuoteToApprove source) {
        return new QuoteDTO.QuoteDTOBuilder()
                .setId(source.getId())
                .setAuthor(source.getAuthor())
                .setQuote(source.getQuote())
                .setSubmitter(source.getSubmitter())
                .createQuoteDTO();
    }
}
