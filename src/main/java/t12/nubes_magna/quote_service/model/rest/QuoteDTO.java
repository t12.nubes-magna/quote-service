package t12.nubes_magna.quote_service.model.rest;

import java.io.Serializable;

public class QuoteDTO implements Serializable {

    private static final long serialVersionUID = 5886438709016405903L;

    private Integer id;
    private String quote;
    private String author;
    private String submitter;

    public QuoteDTO(final Integer id, final String quote, final String author, final String submitter) {
        this.id = id;
        this.quote = quote;
        this.author = author;
        this.submitter = submitter;
    }

    private QuoteDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(final String quote) {
        this.quote = quote;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(final String author) {
        this.author = author;
    }

    public String getSubmitter() {
        return submitter;
    }

    public void setSubmitter(final String submitter) {
        this.submitter = submitter;
    }

    public static class QuoteDTOBuilder {

        private Integer id;
        private String quote;
        private String author;
        private String submitter;

        public QuoteDTOBuilder setId(final Integer id) {
            this.id = id;
            return this;
        }

        public QuoteDTOBuilder setQuote(final String quote) {
            this.quote = quote;
            return this;
        }

        public QuoteDTOBuilder setAuthor(final String author) {
            this.author = author;
            return this;
        }

        public QuoteDTOBuilder setSubmitter(final String submitter) {
            this.submitter = submitter;
            return this;
        }

        public QuoteDTO createQuoteDTO() {
            return new QuoteDTO(id, quote, author, submitter);
        }
    }
}
