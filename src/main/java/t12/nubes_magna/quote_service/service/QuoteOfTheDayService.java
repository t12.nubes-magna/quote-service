package t12.nubes_magna.quote_service.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import t12.nubes_magna.quote_service.dao.QuoteDAO;
import t12.nubes_magna.quote_service.model.ApprovedQuote;

import java.util.List;
import java.util.Random;

@Service
public class QuoteOfTheDayService {

    @Autowired
    private QuoteDAO quoteDAOImpl;

    private ApprovedQuote quoteOfTheDay;


    public ApprovedQuote getQuoteOfTheDay() {
        return quoteOfTheDay;
    }

    @Scheduled(fixedRate = 10000)/*cron="0 0 0 1/1 * ? *"*/
    private void setQuoteOfTheDay() {

        final List<ApprovedQuote> allApprovedQuotes = quoteDAOImpl.getAllApprovedQuotes();

        Random random = new Random();

        final int i = random.nextInt(allApprovedQuotes.size() - 1) + 1;

        this.quoteOfTheDay = allApprovedQuotes.get(i);
    }

}
