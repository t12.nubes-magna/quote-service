DROP TABLE IF EXISTS ApprovedQuotes;
CREATE TABLE ApprovedQuotes (
  id        INT          NOT NULL AUTO_INCREMENT,
  quote     VARCHAR(512) NOT NULL,
  author    VARCHAR(255) NOT NULL,
  submitter VARCHAR(255) NOT NULL,
  lastUsed  DATE,
  PRIMARY KEY (id)
);

INSERT INTO ApprovedQuotes (quote, author, submitter, lastUsed) VALUES
  ('The meek shall inherit the Earth, but not its mineral rights.', 'J. Paul Getty', 'admin', NULL),
  ('Make crime pay. Become a lawyer.', 'Will Rogers', 'admin', NULL),
  ('I can speak Esperanto like a native.', 'Spike Milligan', 'admin', NULL),
  ('Public opinion is no more than this: what people think that other people think.', 'Alfred Austin', 'admin', NULL),
  ('Thinking is one thing no one has ever been able to tax.', 'Charles Kettering', 'admin', NULL),
  ('Do not take life too seriously. You will never get out of it alive.', 'Elbert Hubbard', 'admin', NULL),
  ('I love deadlines. I like the whooshing sound they make as they fly by.', 'Douglas Adams', 'admin', NULL),
  ('I believe that if life gives you lemons, you should make lemonade... And try to find somebody whose life has given them vodka, and have a party.', 'Ron White', 'admin', NULL),
  ('Go to Heaven for the climate, Hell for the company', 'Mark Twain', 'admin', NULL),
  ('Procrastination is the art of keeping up with yesterday.', 'Don Marquis', 'admin', NULL),
  ('The best ideas come as jokes. Make your thinking as funny as possible.', 'David Ogilvy', 'admin', NULL),
  ('There cannot be a crisis next week. My schedule is already full.', 'Henry A. Kissinger', 'admin', NULL),
  ('If at first you don''t succeed... so much for skydiving.', 'Henny Youngman', 'admin', NULL),
  ('Reality continues to ruin my life.', 'Bill Watterson', 'admin', NULL),
  ('If you want a guarantee, buy a toaster.', 'Clint Eastwood', 'admin', NULL),
  ('I cook with wine, sometimes I even add it to the food.', 'W. C. Fields', 'admin', NULL),
  ('Everybody talks about the weather, but nobody does anything about it.', 'Charles Dudley Warner', 'admin', NULL),
  ('If you don''t mind, it doesn''t matter.', 'Jack Benny', 'admin', NULL),
  ('What''s another word for Thesaurus?', 'Steven Wright', 'admin', NULL),
  ('If I studied all my life, I couldn''t think up half the number of funny things passed in one session of congress.', 'Will Rogers', 'admin', NULL),
  ('You know an odd feeling? Sitting on the toilet eating a chocolate candy bar.', 'George Carlin', 'admin', NULL),
  ('Atheism is a non-prophet organization.', 'George Carlin', 'admin', NULL),
  ('Inside every cynical person, there is a disappointed idealist.', 'George Carlin', 'admin', NULL),
  ('The reason I talk to myself is that I''m the only one whose answers I accept.', 'George Carlin', 'admin', NULL),
  ('Most people work just hard enough not to get fired and get paid just enough money not to quit.', 'George Carlin',
   'admin', NULL);


DROP TABLE IF EXISTS QuotesToApprove;
CREATE TABLE QuotesToApprove (
  id        INT          NOT NULL AUTO_INCREMENT,
  quote     VARCHAR(512) NOT NULL,
  author    VARCHAR(255) NOT NULL,
  submitter VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO QuotesToApprove (quote, author, submitter) VALUES
  ('Do not take life too seriously. You will never get out of it alive.', 'Elbert Hubbard', 'admin'),
  ('As for our majority... one is enough.', 'Benjamin Disraeli', 'admin'),
  ('Tech is a funny industry; I don''t think there is any other industry on the planet that reinvents itself every 10-12 years.',
    'Edward Zander', 'admin'),
  ('I distrust camels, and anyone else who can go a week without a drink.', 'Joe E. Lewis', 'admin'),
  ('Insanity: doing the same thing over and over again and expecting different results', 'Albert Einstein', 'admin'),
  ('Get your facts first, then you can distort them as you please.', 'Mark Twain', 'admin');
