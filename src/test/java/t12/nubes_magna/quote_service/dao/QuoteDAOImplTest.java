package t12.nubes_magna.quote_service.dao;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;
import t12.nubes_magna.quote_service.model.ApprovedQuote;
import t12.nubes_magna.quote_service.model.QuoteToApprove;

@SpringBootTest
public class QuoteDAOImplTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private QuoteDAO quoteDAOImpl;


    @Test
    public void getAllApprovedQuotes() throws Exception {

        Assert.assertTrue(!quoteDAOImpl.getAllApprovedQuotes().isEmpty());
    }

    @Test
    public void addNewQuote() throws Exception {

        QuoteToApprove quoteToApprove = new QuoteToApprove.QuoteToApproveBuilder().setQuote("test quote")
                .setAuthor("test author").setSubmitter("test submitter").createQuote();
        final QuoteToApprove insertedQuote = quoteDAOImpl.addNewQuote(quoteToApprove);
        Assert.assertEquals(quoteToApprove.getQuote(), insertedQuote.getQuote());
        Assert.assertEquals(quoteToApprove.getAuthor(), insertedQuote.getAuthor());
        Assert.assertEquals(quoteToApprove.getSubmitter(), insertedQuote.getSubmitter());
    }

    @Test
    public void getQuotesToApprove() throws Exception {
        Assert.assertTrue(!quoteDAOImpl.getQuotesToApprove().isEmpty());
    }

    @Test
    public void approveQuote() throws Exception {
        QuoteToApprove quoteToApprove = new QuoteToApprove.QuoteToApproveBuilder().setQuote("test quote")
                .setAuthor("test author").setSubmitter("test submitter").createQuote();
        final QuoteToApprove insertedQuote = quoteDAOImpl.addNewQuote(quoteToApprove);

        final ApprovedQuote approvedQuote = quoteDAOImpl.approveQuote(insertedQuote.getId());

        Assert.assertNotNull(approvedQuote);
    }

}